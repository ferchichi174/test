<?php

namespace App\DataFixtures;

use App\Entity\Items;
use App\Entity\Meals;
use App\Entity\Menu;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->Userload($manager);
        $this->Itemsload($manager);
        $this->Mealsload($manager);
        $this->Menuload($manager);

    }
    private function Userload(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');
        for ($i=1;$i<=7;$i++) {

            $user = new User();
            $user->setEmail($faker->email);
            $user->setFirstname($faker->firstName);
            $user->setLastname($faker->lastName);
            $user->setPassword($this->encoder->encodePassword($user,'demo'));
            $manager->persist($user);
            $this->addReference('user'.$i,$user);
        }
        $manager->flush();

    }


    private function Menuload(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();

        for ($i=1;$i<=7;$i++){
            $Menu = new Menu();
            $Menu->setTitle($faker->name);
            $types=["Food","Cold Drink","Hot Drink"];
            $Menu->setType($types[rand(0,2)]);
            $Menu->setDescription($faker->paragraph());
            $Menu->setImage("menu".$i.".jpg");
            $user =   $this->getReference("user" . rand(1,7));
            $Menu->setIsBest(rand(0,1));
            $Menu->setUser($user);
            $manager->persist($Menu);

        }
        $manager->flush();
    }

    private function Itemsload(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();
        for ($i=1;$i<=7;$i++){

            $Items = new Items();
            $Items->settitle($faker->name);
            $Items->setDescription($faker->paragraph());
            $user =   $this->getReference("user" . rand(1,7));
            $Items->setUser($user);
            $Items->setPrice(2000+$i);
            $Items->setImage("food".$i.".jpg");
            $this->addReference('items'.$i,$Items);

            $manager->persist($Items);

        }
        $manager->flush();
    }
    private function Mealsload(ObjectManager $manager)

    {
        $faker = \Faker\Factory::create();
        for ($i=1;$i<=7;$i++){
            $Meals = new Meals();
            $Meals->setTitle($faker->name);
            $Meals->setDescription($faker->paragraph());
            $Meals->setImage("menu".$i.".jpg");
            $Meals->setPrice($faker->randomFloat());
            $user =   $this->getReference("user" . rand(1,7));
            $Meals->setUser($user);
            $items =   $this->getReference("items" . rand(1,7));
            $Meals->addItem($items);


            $manager->persist($Meals);
        }

        $manager->flush();
    }







}
