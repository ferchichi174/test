<?php

namespace App\Form;

use App\Entity\User;
use phpDocumentor\Reflection\Types\True_;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email',EmailType::class,['disabled'=> true,'label'=>'Mon adresse email'])
            ->add('firstname',TextType::class,['disabled'=> true,'label'=>'Mon Prenom'])
            ->add('lastname',TextType::class,['disabled'=> true,'label'=>'Mon Nom'])
            ->add('old_password',PasswordType::class,['label'=>'Mon mot de passe actuel','mapped'=> false ])
            ->add('new_password',RepeatedType::class,['type'=>PasswordType::class,
                'invalid_message'=>'Le mot de passe et la confirmation doive etre identique',
                'mapped'=> false,
                'label'=>'Votre mot de passe',
                'required'=>True,
                'first_options'=>['label'=>'Mon nouveau mot de passe'],
                'second_options'=>['label'=>'Confirmer la nouveau mot de passe']

            ])
            ->add('submit',SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
