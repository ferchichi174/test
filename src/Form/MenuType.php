<?php

namespace App\Form;

use App\Entity\Adress;
use App\Entity\Items;
use App\Entity\Menu;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
           ->add('type', ChoiceType::class, [
               'choices' => [
                   'Food' => 1,
                   'Hot drink' => 2,
                   'Cold dring' => 3,]]
           )
            ->add('description')
            ->add('image',FileType::class,array('data_class'=>Null,'required'=>false))

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);
    }
}
