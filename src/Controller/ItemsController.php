<?php

namespace App\Controller;

use App\Entity\Items;
use App\Form\ItemsType;
use App\Repository\ItemsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/account/items")
 */
class ItemsController extends AbstractController
{
    /**
     * @Route("/", name="items_index", methods={"GET"})
     */
    public function index(ItemsRepository $itemsRepository,PaginatorInterface $paginator,Request $request): Response
    {
        $donnee = $itemsRepository->findBy(array(), array('id' => 'desc'));
        $item=  $paginator->paginate($donnee,$request->query->getInt('page',1),6);
        return $this->render('items/index.html.twig', [
            'items' => $item
        ]);
    }

    /**
     * @Route("/new", name="items_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $item = new Items();
        $form = $this->createForm(ItemsType::class, $item);
        $name = $item->getImage();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($item->getImage()==""){
                $item->setImage("no_image.png");
            }
            else {

                $file = new File($item->getImage());
                $filename= md5(uniqid()).'.'.$file->guessExtension();

                $file->move($this->getParameter('upload_directory'),$filename);
                $item->setImage($filename);
            }
            $item->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($item);
            $entityManager->flush();

            return $this->redirectToRoute('items_index');
        }

        return $this->render('items/new.html.twig', [
            'item' => $item,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="items_show", methods={"GET"})
     */
    public function show(Items $item): Response
    {
        return $this->render('items/show.html.twig', [
            'item' => $item,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="items_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Items $item): Response
    {
        $name = $item->getImage();
        $form = $this->createForm(ItemsType::class, $item);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            if ($item->getImage()==""){

                $item->setImage($name);
            }
            else {

                $file = new File($item->getImage());
                $filename= md5(uniqid()).'.'.$file->guessExtension();

                $file->move($this->getParameter('upload_directory'),$filename);
                $item->setImage($filename);
                if($name !="no_image.jpg"){
                    if(file_exists("img/".$name)){
                        unlink("img/".$name);
                    }
                }

            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('items_index');
        }

        return $this->render('items/edit.html.twig', [
            'item' => $item,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="items_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Items $item): Response
    {
        if ($this->isCsrfTokenValid('delete'.$item->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('items_index');
    }
}
