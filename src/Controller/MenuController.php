<?php

namespace App\Controller;

use App\Entity\Menu;
use App\Form\MenuType;
use App\Repository\MenuRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/account/menu")
 */
class MenuController extends AbstractController
{


    private  $entityManager ;

    public  function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    /**
     * @Route("/", name="menu_index", methods={"GET"})
     */
    public function index(MenuRepository $menuRepository,PaginatorInterface $paginator,Request $request): Response
    {
        $donnee=  $menuRepository->findby(array(), array('id' => 'desc'));
        $menu = $paginator->paginate($donnee,$request->query->getInt('page',1),6);
        return $this->render('menu/index.html.twig', [
            'menus' =>$menu
        ]);
    }

    /**
     * @Route("/new", name="menu_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $menu = new Menu();
        $name = $menu->getImage();
        $form = $this->createForm(MenuType::class, $menu);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($menu->getImage()==""){
                $menu->setImage("no_image.png");
            }
            else {

                $file = new File($menu->getImage());
                $filename= md5(uniqid()).'.'.$file->guessExtension();

                $file->move($this->getParameter('upload_directory'),$filename);
                $menu->setImage($filename);

            }
            $date = new \DateTime();
            $user =$this->getUser();
            $menu->setUser($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($menu);
            $entityManager->flush();

            return $this->redirectToRoute('menu_index');
        }

        return $this->render('menu/new.html.twig', [
            'menu' => $menu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="menu_show", methods={"GET"})
     */
    public function show(Menu $menu): Response
    {

        return $this->render('menu/show.html.twig', [
            'menu' => $menu,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="menu_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Menu $menu): Response
    {
        $name = $menu->getImage();
        $form = $this->createForm(MenuType::class, $menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() ) {

            if ($menu->getImage()== ""){

                $menu->setImage($name);
            }
            else {

                $file = new File($menu->getImage());
                $filename= md5(uniqid()).'.'.$file->guessExtension();

                $file->move($this->getParameter('upload_directory'),$filename);
                $menu->setImage($filename);
                if($name !="no_image.jpg"){
                    if(file_exists("img/".$name)){
                        unlink("img/".$name);
                    }
                }
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('menu_index');
        }

        return $this->render('menu/edit.html.twig', [
            'menu' => $menu,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="menu_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Menu $menu): Response
    {
        if ($this->isCsrfTokenValid('delete'.$menu->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($menu);
            $entityManager->flush();
        }

        return $this->redirectToRoute('menu_index');
    }
}
