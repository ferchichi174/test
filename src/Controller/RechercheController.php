<?php

namespace App\Controller;

use App\Repository\ItemsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RechercheController extends AbstractController
{
    /**
     * @Route("/recherche", name="recherche")
     */
    public function index(Request $request, ItemsRepository $Item): Response
    {
        if ($request->getMethod() == "POST"){
            $shearch=$request->request->get("search");
              $go=  $Item->findItemByTitle($shearch);
        }
        return $this->render('recherche/index.html.twig', [
            'items' => $go,
        ]);
    }
}
