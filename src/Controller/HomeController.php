<?php

namespace App\Controller;

use App\Repository\HeaderRepository;
use App\Repository\ItemsRepository;
use App\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(HeaderRepository $head,MenuRepository $menu,ItemsRepository $item): Response
    {
        $items =   $item->findBy(['isBest'=>1]);
        $menus = $menu->findBy(['isBest'=>1]);
        $header =$head->findAll();
        return $this->render('home/index.html.twig', [
            'headers' => $header,
            'menus' =>$menus,
            'items' => $items
        ]);
    }
}
