<?php

namespace App\Controller;

use App\Entity\Meals;
use App\Form\MealsType;
use App\Repository\MealsRepository;
use App\Repository\MenuRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/account/meals")
 */
class MealsController extends AbstractController
{
    /**
     * @Route("/", name="meals_index", methods={"GET"})
     */
    public function index(MealsRepository $mealsRepository,PaginatorInterface $paginator,Request $request): Response
    {
       $donnee= $mealsRepository->findBy(array(), array('id' => 'desc'));
        $meals=  $paginator->paginate($donnee,$request->query->getInt('page',1),6);
        return $this->render('meals/index.html.twig', [
            'meals' =>$meals,

        ]);
    }

    /**
     * @Route("/new", name="meals_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {

        $meal = new Meals();
        $name = $meal->getImage();
        $form = $this->createForm(MealsType::class, $meal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($meal->getImage()==""){
                $meal->setImage("no_image.png");

            }
            else {

                $file = new File($meal->getImage());
                $filename= md5(uniqid()).'.'.$file->guessExtension();

                $file->move($this->getParameter('upload_directory'),$filename);
                $meal->setImage($filename);

            }

            $meal->SetUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($meal);
            $entityManager->flush();

            return $this->redirectToRoute('meals_index');
        }

        return $this->render('meals/new.html.twig', [
            'meal' => $meal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="meals_show", methods={"GET"})
     */
    public function show(Meals $meal,MenuRepository $menu): Response
    {
        $menus = $menu->findBy(['isBest'=>1]);
        return $this->render('meals/show.html.twig', [
            'meal' => $meal,
            'menus' =>$menus
        ]);
    }

    /**
     * @Route("/{id}/edit", name="meals_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Meals $meal): Response
    {
        $name = $meal->getImage();
        $form = $this->createForm(MealsType::class, $meal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($meal->getImage() =="") {
                $meal->setImage($name);
            } else {

                $file = new File($meal->getImage());
                $filename = md5(uniqid()) . '.' . $file->guessExtension();

                $file->move($this->getParameter('upload_directory'), $filename);
                $meal->setImage($filename);
                if($name !="no_image.jpg"){
                    if(file_exists("img/".$name)){
                        unlink("img/".$name);
                    }
                }
            }
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('meals_index');
            }

        return $this->render('meals/edit.html.twig', [
            'meal' => $meal,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="meals_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Meals $meal): Response
    {
        if ($this->isCsrfTokenValid('delete'.$meal->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($meal);
            $entityManager->flush();
        }

        return $this->redirectToRoute('meals_index');
    }
}
