<?php

namespace App\Controller;

use App\Repository\MenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShowController extends AbstractController
{
    /**
     * @Route("/{id}/show", name="show")
     */
    public function index($id,MenuRepository $menus): Response
    {
       $menu= $menus->findOneBy(['id'=>$id]);
        return $this->render('show/index.html.twig', [
            'menu' => $menu,
        ]);
    }
}
